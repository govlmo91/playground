package datastructure;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class MySingleLinkedListTest {

    @Test
    public void test() {
        MySingleLinkedList<String> list = new MySingleLinkedList<>();

        list.add(0, "Saturday");
        list.add(1, "Friday");
        list.add(0, "Monday"); // Monday, Saturday. Friday
        list.add(2, "Tuesday"); // Monday, Saturday, Tuesday, Friday

        String str = list.get(2);//Tuesday
        assertEquals("Tuesday", str);

        list.remove(2); // Monday, Saturday, Friday
        assertEquals(3, list.getSize());
        assertEquals("Monday", list.get(0));
        assertEquals("Saturday", list.get(1));
        assertEquals("Friday", list.get(2));

        int pos = list.indexOf("Friday"); // 2
        assertEquals(2, pos);
    }

    @Test
    public void add_메소드() {
        MySingleLinkedList<String> linkedList = new MySingleLinkedList<>();
        linkedList.addFirst("AAA");
        linkedList.addFirst("AA");
        linkedList.addFirst("A");

        assertTrue(linkedList.getSize() == 3);
        assertEquals("A", linkedList.get(0));
        assertEquals("AA", linkedList.get(1));
        assertEquals("AAA", linkedList.get(2));

        String s = linkedList.removeFirst();
        assertEquals("A", s);
        assertEquals("AA", linkedList.get(0));
        assertEquals("AAA", linkedList.get(1));
    }

    @Test
    public void remove_메소드() {
        MySingleLinkedList<String> linkedList = new MySingleLinkedList<>();
        linkedList.addFirst("AAA");
        linkedList.addFirst("AA");
        linkedList.addFirst("A");

        assertTrue(linkedList.get(0).equals("A"));
        assertTrue(linkedList.get(1).equals("AA"));
        assertTrue(linkedList.get(2).equals("AAA"));


        linkedList.remove(0);
        assertTrue(linkedList.getSize() == 2);
        assertTrue(linkedList.get(0).equals("AA"));
        assertTrue(linkedList.get(1).equals("AAA"));

        linkedList.remove(1);
        assertTrue(linkedList.get(0).equals("AA"));
        assertTrue(linkedList.getSize()==1);
    }




}