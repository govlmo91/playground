package algorithm.chap02;

import java.util.Arrays;

public class CloneArray {
    public static void main(String[] args) {
        int[] a = {1,2,3,4,5};
        int[] b = a.clone();

        b[3] = 0;

        System.out.println("a = " + Arrays.toString(a));
        System.out.println("b = " + Arrays.toString(b));

        /////

        Integer[] aa = {1,2,3,4,5};
        Integer[] bb = aa.clone();

        bb[3] = 1111;

        System.out.println("aa = " + Arrays.toString(aa));
        System.out.println("bb = " + Arrays.toString(bb));

    }
}
