package algorithm.chap02;

import java.util.Arrays;

public class ReversArray {
    public static void main(String[] args) {
        int[] a = {22, 57, 11, 32, 91, 68, 70};

        for(int i = 0; i < a.length/2; i++) {
            swap(a, i, a.length - i - 1);
        }

        System.out.println("Arrays.toString(a) = " + Arrays.toString(a));

    }

    static void swap(int[] a, int idx1, int idx2) {
        int temp = a[idx1];
        a[idx1] = a[idx2];
        a[idx2] = temp;
    }
}
