package algorithm.chap01;

public class Max3m {
    public static void main(String[] args) {
        int i = max4(1, 6, 4, 2);
        System.out.println("i = " + i);

        int i1 = min3(4, 2, 7);
        System.out.println("i1 = " + i1);
    }
    static int max3(int a, int b, int c) {
        int max = a;
        if(b > max) {
            max = b;
        }
        if(c > max) {
            max = c;
        }

        return max;
    }

    static int max4(int a, int b, int c, int d) {
        int max = a;

        if(b > max) {
            max = b;
        }
        if(c > max) {
            max = c;
        }
        if(d > max) {
            max = d;
        }

        return max;
    }

    static int min3(int a, int b, int c) {
        int min = a;

        if(b < min) {
            min = b;
        }

        if(c < min) {
            min = c;
        }
        return min;
    }
}
