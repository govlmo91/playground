package algorithm.chap01;

import java.util.Scanner;

public class SumWhile {
    public static void main(String[] args) {
        int n = 7;
        System.out.println("1부터 n까지의 합");

        int sum = 0;
        for(int i = 1; i <= n; i++) {
            sum += i;
        }

        System.out.println("sum = " + sum);

        int sum2 = sumOf(6, 4);
        System.out.println("sum2 = " + sum2);
    }

    static int sumOf(int a, int b) {
        //a와 b사이의 합
        int sum = 0;
        int temp = 0;
        if( b < a) {
            temp = b;
            b = a;
            a = temp;
        }

        for(int i = a; i <= b; i++) {
            sum += i;
        }

        return sum;
    }
}
