package algorithm.chap03;

/**
 * 이진검색.
 */
public class BinSearch {
    // 요솟수가 n인 배열 a에서 key와 같은 요소를 이진검색한다.
    static int binSearch(int[] a, int n, int key) {
        int pl = 0; //검색 범위의 첫 인덱스.
        int pr = n - 1; //검색 범위의 끝 인덱스.

        do {
            int pc = (pl + pr) / 2; //중앙요소의 인덱스

            if (a[pc] == key) {
                return pc;
            } else if (a[pc] < key) {
                pl = pc + 1;
            } else {
                pr = pc - 1; // 검색 범위를 앞쪽 절반으로 좁힘.
            }
        } while (pl <= pr);

        return -1; //검색실패.
    }
}
