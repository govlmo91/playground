package algorithm.tree;

public class Tree {
    public static void main(String[] args) {
        for (int i = 0; i < 9; i++) {
            System.out.println();
            for (int j = 11; j >= i; j--) {
                System.out.print(" ");
            }
            for (int k = 0; k <= i; k++) {
                System.out.print("* ");
            }
        }
        System.out.println("\n            ||\n            ||");
        System.out.println("      Merry Christmas!!");
    }
}
