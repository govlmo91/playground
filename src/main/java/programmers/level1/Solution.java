package programmers.level1;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int i = solution.solution1(20);
        System.out.println("i = " + i);
    }

    public int solution1(int n) {
        int count = 0;

        for (int i = 0; i < n; i++) {
            int result = i;

            for (int j = i + 1; result < n; j += 1) {
                result += j;

                if (result == n) {
                    count += 1;
                    break;
                }
            }
        }
        return count;
    }
}
