package datastructure;

public class MySingleLinkedList<T> {

    private Node<T> head;
    private int size;

    public int getSize() {
        return size;
    }

    public MySingleLinkedList() {
        head = null;
        size = 0;
    }

    public void addFirst(T item) {
        Node<T> newNode = new Node<T>();
        newNode.data = item;
        newNode.next = head;
        head = newNode;
        size++;
    }

    public void addAfter( Node<T> before, T item ) {
        Node<T> temp = new Node<T>();
        temp.data = item;
        temp.next = before.next;
        before.next = temp;
        size++;
    }

    public T removeFirst() {
        assert head != null;
        T temp = head.data;
        head = head.next;
        size--;
        return temp;
    }

    public T removeAfter(Node<T> before) {
        assert before.next != null;
        T data = before.next.data;
        before.next = before.next.next;
        size--;
        return data;
    }

    public Node<T> getNode(int index) {
        if(index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        Node<T> currentNode = head;

        for(int i = 0; i < index; i++) {
            currentNode = currentNode.next;
        }

        return currentNode;
    }

    public void add(int index, T item) {
        if(index < 0) {
            throw new IndexOutOfBoundsException();
        }
        if(index == 0) {
            addFirst(item);
            return;
        }
        Node<T> before = getNode(index-1);
        addAfter(before, item);
    }

    public T remove(int index) {
        if(index < 0) {
            throw new IndexOutOfBoundsException();
        }
        if(index == 0) {
            return removeFirst();
        }
        return removeAfter(getNode(index-1));
    }

    public T remove(T item) {
        int index = indexOf(item);
        if( index == -1 ) throw new IllegalArgumentException();

        return remove(index);
    }

    public T get(int index) {
        Node<T> node = getNode(index);
        return node.data;
    }

    public int indexOf(T item) {
        Node<T> current = head;
        int index = 0;

        while(current != null) {
            if(current.data.equals(item)) {
                return index;
            }
            current = current.next;
            index++;
        }

        return -1;
    }
        static class Node<T> {
            public T data;
            public Node<T> next;

            @Override
            public String toString() {
                return "Node{" +
                        "data=" + data +
                        '}';
            }
        }
}
