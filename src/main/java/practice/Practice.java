package practice;

public class Practice {
    public static void main(String[] args) {
        int[] arr = new int[10];

        arr[0] = 1;
        arr[1] = 2;
        arr[2] = 3;
        arr[3] = 4;

        for(int num : arr) {
            System.out.println(num);
        }
    }

}
